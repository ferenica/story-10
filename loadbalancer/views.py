from django.shortcuts import render

def index(request):

    host = request.get_host()
    port = request.get_port()

    context = {
        'host':host,
        'port':port,
    }

    return render(request, 'index.html', context)
